import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../services/auth/auth.service';
import { CommonService } from '../services/common/common.service';
import { HttpService } from '../services/http/http.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationdialogComponent } from '../confirmationdialog/confirmationdialog.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  roles = ['Admin', 'Loading Operator'];
  currentOperator: any;
  loginForm: FormGroup | any;
  signupForm: FormGroup | any;
  showPassword: boolean = false;
  currentUser: any;
  isSignUp = false;
  hide = true;
  hide1 = true;
  isexist = false;
  isdoesnotexist = false;
  isLoading = false;
  constructor(public router: Router, private fb: FormBuilder, private authService: AuthService, private httpService: HttpService, private common: CommonService, private dialog: MatDialog) {
    this.loginForm = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.required]
      // operator: ['', Validators.required]
    })
    // this.signupForm = this.fb.group({
    //   username: ['', Validators.required],
    //   firstname: ['', Validators.required],
    //   lastname: ['', Validators.required],
    //   operator: ['', Validators.required],
    //   pwd: ['', Validators.required]
    // })

  }

  ngOnInit(): void {

  }
  loginuser() {
    // this.router.navigate(['home/loading/skuscan'])
    this.isLoading = true;
    let body: any = {};
    body.UserName = this.loginForm.get('email').value;
    // body.Password = this.loginForm.get('pw').value;
    localStorage.setItem('user', this.loginForm.get('email').value);
    localStorage.setItem('fname', this.loginForm.get('firstname').value);
    localStorage.setItem('lname', this.loginForm.get('lastname').value);

    this.router.navigate(['home/loading/skuscan'])

    // this.httpService.post('GetUserData', body).subscribe((res: any) => {
    //   if (res.RoleTypeId == 1 || res.RoleTypeId == 2) {
    //     this.isLoading = false;
    //     this.authService.login(res.RoleTypeId == 1 ? 'Admin' : 'Loading Operator').subscribe(res => {
    //       if (res.role == 'Admin') {

    //         this.router.navigate(['home/loading/skuscan']);

    //       }
    //       else if (res.role == 'Loading Operator') {
    //         this.router.navigate(['home/loading/skuscan']);
    //       }
    //     })

    //     this.common.userId = res['UserId'];
    //     this.router.navigate(['home/loading/skuscan'])
    //   }
    //   else {
    //     this.isLoading = false;
    //     this.isdoesnotexist = true;
    //   }
    // }, err => {
    //   this.isLoading = false;
    //   this.dialog.open(ConfirmationdialogComponent, {
    //     data: 'something'
    //   });
    // })







    // this.currentOperator = this.loginForm.get('operator').value;
    // this.currentUser = this.loginForm.get('user').value;
    // localStorage.setItem('user', this.currentUser);
    // this.authService.login(this.currentOperator).subscribe(res => {
    //   if (res.role == 'Admin') {
    //     // this.router.navigate(['home/masterData'])
    //     this.router.navigate(['home/loading/skuscan']);

    //   }
    //   if (res.role == 'Loading Operator') {
    //     this.router.navigate(['home/loading/skuscan']);
    //   }
    //   else if (res.role == 'Vision Operator') {
    //     this.router.navigate(['home/vision/bigscreen']);
    //   }
    //   // else if (res.role == 'Admin') {
    //   //   this.router.navigate(['home'])
    //   // }
    // })
  }
  signup() {
    this.isSignUp = true;

  }
  signUpUser() {
    let body: any = {};
    body.UserName = this.signupForm.get('username').value;
    body.FirstName = this.signupForm.get('firstname').value;
    body.LastName = this.signupForm.get('lastname').value;
    body.Password = this.signupForm.get('pwd').value;
    body.RoleTypeId = this.signupForm.get('operator').value == 'Admin' ? 1 : 2;
    this.httpService.post('CreateNewUser', body).subscribe((res: any) => {
      if (res['UserId'] != -1) {
        this.common.userId = res['UserId'];
        this.isSignUp = false;
      }
      else {
        this.isexist = true;
      }
    })
    // this.isSignUp = false;

  }
  // loginuser() {
  //   this.currentOperator = this.loginForm.get('operator').value;
  //   if (this.currentOperator == 'Pallet Operator') {
  //     this.router.navigate(['home/pallet/palletconfiramation'])
  //   }
  //   else if (this.currentOperator == 'Loading Operator') {
  //     this.router.navigate(['home/loading/skuscan']);
  //   }
  //   else if (this.currentOperator == 'Vision Operator') {
  //     this.router.navigate(['home/vision/bigscreen']);
  //   }
  //   else {
  //     this.router.navigate(['/home'])

  //   }
  // }
  navigateLogin() {
    this.isSignUp = false;

  }
}
