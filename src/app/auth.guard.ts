import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {
  // canActivate(
  //   route: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  //   return true;
  // }
  constructor(private authService: AuthService, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let url: string = state.url;
    return this.checkUserLogin(next, url);
  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.canActivate(next, state);
  }
  // canLoad(route: Route, segments: UrlSegment[]): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
  //   return this.checkUserLogin(route);
  // }

  //   canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
  //     if (!this.authService.isAuthorized()) {
  //         this.router.navigate(['login']);
  //         return false;
  //     }

  //     const roles = route.data.roles as Role[];
  //     if (roles && !roles.some(r => this.authService.hasRole(r))) {
  //         this.router.navigate(['error', 'not-found']);
  //         return false;
  //     }

  //     return true;
  // }

  // canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
  //     if (!this.authService.isAuthorized()) {
  //         return false;
  //     }

  //     const roles = route.data && route.data.roles as Role[];
  //     if (roles && !roles.some(r => this.authService.hasRole(r))) {
  //         return false;
  //     }

  //     return true;
  // }


  checkUserLogin(route: ActivatedRouteSnapshot, url: any): boolean {
    if (this.authService.isLoggedIn()) {
      const userRole = this.authService.getRole();
      if (route.data['role'] && route.data['role'].indexOf(userRole) === -1) {
        this.router.navigate(['home/noaccess']);
        return false;
      }
      return true;
    }

    this.router.navigate(['home/noaccess']);
    return false;
  }
}
