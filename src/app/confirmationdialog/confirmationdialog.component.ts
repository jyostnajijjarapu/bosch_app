import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../confirm-load-data/confirm-load-data.component';

@Component({
  selector: 'app-confirmationdialog',
  templateUrl: './confirmationdialog.component.html',
  styleUrls: ['./confirmationdialog.component.scss']
})
export class ConfirmationdialogComponent implements OnInit {
  buttonLabes = ['Yes', 'No'];
  dialogdata: any;
  constructor(private dialogRef: MatDialogRef<ConfirmationdialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit(): void {
    this.dialogdata = this.data

  }
  onConfirmClick() {
    this.dialogRef.close(true);

  }
}
