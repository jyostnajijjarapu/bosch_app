import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { LoadingComponent } from './loading/loading.component';
import { ReportsComponent } from './loading/reports/reports.component';
import { ConfirmationdialogComponent } from '../confirmationdialog/confirmationdialog.component';
import { MaterialModule } from '../material.module';
import { MasterDataComponent } from './master-data/master-data.component';
import { HelpComponent } from './help/help.component';


@NgModule({
  declarations: [
    LoadingComponent,
    ReportsComponent,
    ConfirmationdialogComponent,
    MasterDataComponent,
    HelpComponent,


  ],
  imports: [
    CommonModule,
    HomeRoutingModule, MaterialModule
  ]
})
export class HomeModule { }
