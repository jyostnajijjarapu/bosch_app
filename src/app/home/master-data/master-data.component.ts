import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-master-data',
  templateUrl: './master-data.component.html',
  styleUrls: ['./master-data.component.scss']
})
export class MasterDataComponent implements OnInit {
  columnsList = ['FG Part Number (Ex:0261.S12.369-24V)', 'FG Part Numb (Ex:0261S12369)',
    'Box/Bin Qty (Ex:20)',
    'Pallet Qty (Ex:140)',
  ]
  filename: any;
  constructor() { }

  ngOnInit(): void {
  }
  onFileSelect(event: any) {
    this.filename = event.target.files[0].name
  }
}
