import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth.guard';
import { MasterDataComponent } from './master-data/master-data.component';
import { HomeComponent } from './home.component';
import { NoaccessComponent } from './noaccess/noaccess.component';
import { HelpComponent } from './help/help.component';

const routes: Routes = [

  {
    path: '', component: HomeComponent,

    children: [

      {
        path: 'masterData', component: MasterDataComponent,
        // canActivate: [AuthGuard],
        // data: {
        //   role: ['Admin']
        // }
      },
      {
        path: 'help', component: HelpComponent,
        // canActivate: [AuthGuard],
        // data: {
        //   role: ['Loading Operator', 'Admin']
        // }
      },
      {
        path: 'loading',
        loadChildren: () => import('../home/loading/loading.module').then(m => m.LoadingModule),
        // canActivate: [AuthGuard],
        // data: {
        //   role: ['Loading Operator', 'Admin']
        // }

      },

      // {
      //   path: 'vision',
      //   loadChildren: () => import('../home/vision/vision.module').then(m => m.VisionModule),
      //   canActivate: [AuthGuard],
      //   data: {
      //     role: ['Vision Operator', 'Admin']
      //   }

      // },

      // {
      //   path: 'pallet',
      //   loadChildren: () => import('../home/pallet/pallet.module').then(m => m.PalletModule),
      //   canActivateChild: [AuthGuard],
      //   data: {
      //     role: 'Pallet Operator'
      //   }

      // },

      {
        path: '**', component: NoaccessComponent
      }

    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
