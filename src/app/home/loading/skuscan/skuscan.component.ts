import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatTabBody } from '@angular/material/tabs';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/services/http/http.service';
import * as moment from 'moment';
import { ConfirmationdialogComponent } from 'src/app/confirmationdialog/confirmationdialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-skuscan',
  templateUrl: './skuscan.component.html',
  styleUrls: ['./skuscan.component.scss']
})
export class SkuscanComponent implements OnInit {
  part_num = '';
  dateValues: any = [];
  shiftValues: any = [];
  partNumberValues: any = [];
  palletIdValues: any = [];
  statusValues: any = [];
  isShow = false;
  isShow1 = false;
  isShow2 = false;
  // isShow3 = false;
  isShow4 = false;
  isClear = true;
  start: any = '';
  end: any = '';
  filteredData: any;
  @ViewChild('matSelect') matSelect: any = null;
  selectedValues: any;
  selectedShiftValues: any = [];
  selectedStatusValues: any = [];
  isLoading = false;
  toppings = new FormControl();
  constructor(private router: Router, private httpService: HttpService, private dialog: MatDialog) { }
  tableData = [];

  tableCols = ['PALLET ID', 'DATE', 'TIME', 'SHIFT', 'PART NUMBER', 'PALLETQTY-PLAN', 'PALLETQTY-ACTUAL', 'NO.OF BOXES', 'STATUS']
  ngOnInit(): void {
    this.isLoading = true;
    this.httpService.get('getlatest').subscribe((res: any) => {
      if (res) {
        this.shiftConversion(res);
        this.tableData = res;
        this.isLoading = false;

      }

    }, err => {
      this.dialog.open(ConfirmationdialogComponent, {
        data: 'something'
      });
      this.isLoading = false;

    })

  }
  scanData() {
    this.router.navigate(['home/loading/loadingview'])
  }
  filterdata(title: any) {


    if (title == 'DATE') {
      this.isShow = !this.isShow;
      this.isShow1 = false; this.isShow2 = false; this.isShow4 = false;
      this.dateValues = [...new Set(this.tableData.map(({ Date }) => Date))];
      this.filteredData = this.dateValues
    }
    else if (title == 'SHIFT') {
      this.isShow1 = !this.isShow1;
      this.isShow = false; this.isShow2 = false; this.isShow4 = false;
      this.shiftValues = [...new Set(this.tableData.map(({ Shift }) => Shift))]
      console.log(this.shiftValues, "dddddddd")
      this.filteredData = this.shiftValues

    }
    else if (title == 'PART NUMBER') {
      this.isShow2 = !this.isShow2;
      this.isShow = false; this.isShow1 = false; this.isShow4 = false;
      this.partNumberValues = [...new Set(this.tableData.map(({ PartNumber }) => PartNumber))];
      this.filteredData = this.partNumberValues;
    }

    else if (title == 'STATUS') {
      this.isShow4 = !this.isShow4;
      this.isShow = false; this.isShow1 = false; this.isShow2 = false;
      this.statusValues = [...new Set(this.tableData.map(({ PalletStatus }) => PalletStatus))];
      this.filteredData = this.statusValues;
    }
  }

  okClicked(title: any) {
    if (title == 'SHIFT') {
      let shiftconvert: any = []
      this.selectedValues.forEach((ele: any) => {
        if (ele == 'A') {
          shiftconvert.push('1')
        }
        else if (ele = 'B') {
          shiftconvert.push('2')
        }
        else {
          shiftconvert.push('3')
        }
      });
      console.log(this.selectedValues, "okhyfevgkfekcihf")
      this.selectedShiftValues = shiftconvert;


    }
    if (title == 'STATUS') {
      this.selectedStatusValues = this.selectedValues;

    }
    this.matSelect.close()
  }
  cancelClicked() {
    this.matSelect.close()
  }
  filterTable() {
    this.isLoading = true;

    this.isClear = false;
    let body: any = {}
    if (this.start != '') {
      let sDate = new Date(this.start).toLocaleString();
      let eDate = new Date(this.end).toLocaleString();
      body['StartDate'] = moment(new Date(sDate.substr(0, 16))).format("YYYY-MM-DD");
      body['EndDate'] = moment(new Date(eDate.substr(0, 16))).format("YYYY-MM-DD");

    }
    if (this.selectedShiftValues.length > 0) {
      console.log(this.selectedShiftValues, "shift")
      body['Shift'] = this.selectedShiftValues;
    }
    if (this.selectedStatusValues.length > 0) {
      body['PalletStatus'] = this.selectedStatusValues;
    }
    if (this.part_num != '') {
      body['PartNumber'] = this.part_num;
    }
    this.httpService.post('GetFilterData', body).subscribe((res: any) => {
      if (res) {
        this.shiftConversion(res);
        this.tableData = res;
        this.isLoading = false;

      }


    }, err => {
      this.dialog.open(ConfirmationdialogComponent, {
        data: 'something'
      });
      this.isLoading = false;

    })


  }
  clearFilter() {
    const dialogRef = this.dialog.open(ConfirmationdialogComponent, {
      data: "Are you sure want to clear the filter?"
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.isClear = true;
        // this.filteredData = []
        this.selectedValues = [];
        this.selectedShiftValues = []; this.selectedStatusValues = []; this.start = ''; this.end = ''; this.part_num = '';
        this.isShow = false; this.isShow1 = false; this.isShow2 = false; this.isShow4 = false;
        this.httpService.get('getlatest').subscribe((res: any) => {
          if (res) {
            this.shiftConversion(res);
            this.tableData = res;
          }

        })
      }
    })

  }
  shiftConversion(res: any) {
    res.forEach((ele: any) => {
      if (ele['Shift'] == '1') {
        ele['Shift'] = 'A';
      }
      if (ele['Shift'] == '2') {
        ele['Shift'] = 'B';
      }
      if (ele['Shift'] == '3') {
        ele['Shift'] = 'C';
      }
    });
  }
}


