import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as d3 from 'd3';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { LoadingviewComponent } from '../loadingview/loadingview.component';
import { HttpService } from 'src/app/services/http/http.service';
import { ConfirmationdialogComponent } from 'src/app/confirmationdialog/confirmationdialog.component';
import { MatDialog } from '@angular/material/dialog';
import { CommonService } from 'src/app/services/common/common.service';
@Component({
  selector: 'app-palletloadstatus',
  templateUrl: './palletloadstatus.component.html',
  styleUrls: ['./palletloadstatus.component.scss'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false },
    },
  ],
})
export class PalletloadstatusComponent implements OnInit {
  // data: any = [
  //   {

  //     value: '2',
  //     color: '#1ABB1D',
  //   },
  //   // {

  //   //   value: '0',
  //   //   color: '#DD1208',
  //   // },
  //   {
  //     // name: 'c',
  //     value: '6',
  //     color: '#CBC2C2',

  //   },
  // ];
  data: any = [];
  // startAngle: -Math.PI/4, endAngle: },
  private margin = { top: 10, right: 30, bottom: 30, left: 40 };
  private width = 250;
  private height = 250;
  private svg: any;
  private colors: any;
  private radius = Math.min(this.width, this.height) / 2 - this.margin.left;
  statuscode: any;
  ws!: WebSocket;
  graphIndex = 1;
  isReject = false;
  isAccept = true;
  isComplete = true;
  constructor(private router: Router, public commonService: CommonService, private httpService: HttpService, private dialog: MatDialog, private ngZone: NgZone) {
    // this.ws = new WebSocket('ws://win-he1pd6l1529:8080/');
    this.ws = new WebSocket('ws://172.16.1.234:8080')
    this.ws.onopen = () => this.ws.send("connected to Server");


    this.ws.onmessage = async (event) => {
      if (event.data) {
        this.commonService.liveData = JSON.parse(event.data);
        if (this.commonService.liveData.PalletStatus !== null) {
          this.commonService.palletstatus = false;
        }
        else {
          this.commonService.palletstatus = true;
        }
        let obj = { value: this.commonService.liveData['ProcessedPalletQty'], color: '#1ABB1D' };
        let obj1 = { value: this.commonService.liveData['PendingPalletQty'], color: '#CBC2C2' };
        let arr = [obj, obj1];
        this.data = arr;
        if (this.commonService?.confirmData?.['PalletQty'] == this.commonService.liveData['ProcessedPalletQty']) {
          this.isAccept = true;
          this.isReject = true;
          this.isComplete = false;
        }
        else if (this.commonService?.confirmData?.['PalletQty'] > this.commonService.liveData['ProcessedPalletQty']) {
          this.isAccept = false;
          this.isReject = false;
          this.isComplete = true;
        }
        if (this.commonService.liveData['IsPalletQtyExceed'] == 1) {

          let dialogRef = this.dialog.open(ConfirmationdialogComponent, {
            data: 'Exceeds pallet quantity!'
          })
          dialogRef.afterClosed().subscribe((confirmed) => {
            if (confirmed) {
              this.ngZone.run(() => {
                dialogRef.close(true);
              });
            }

            // if (confirmed) {
            //   dialogRef.close();
            // }
          })
        }
        d3.selectAll('svg').remove();
        await this.drawChart1(this.data);
      }


    }

  }
  ngOnInit(): void {

    this.commonService.palletstatus = false;
    this.commonService.liveData['ProcessedPalletQty'] = 0;
    this.commonService.liveData['PendingPalletQty'] = this.commonService.confirmData['PalletQty'];
    this.commonService.liveData['TotalPalletQty'] = this.commonService.confirmData['PalletQty'];
    this.commonService.liveData['GoodBoxes'] = 0;
    this.commonService.liveData['BadBoxes'] = 0;
    let obj = { value: this.commonService.liveData['ProcessedPalletQty'], color: '#1ABB1D' };
    let obj1 = { value: this.commonService.liveData['PendingPalletQty'], color: '#CBC2C2' };
    let arr = [obj, obj1];
    this.data = arr;
    d3.selectAll('svg').remove();
    this.drawChart1(this.data);
  }
  private drawChart1(data: any) {

    this.svg = d3
      .select('#donutchart')
      .append('svg')
      .attr('viewBox', `0 0 ${this.width} ${this.height}`)
      .append('g')
      .attr(
        'transform',
        'translate(' + this.width / 2 + ',' + this.height / 2 + ')'
      );
    const colorsRange: any = [];
    this.data.forEach((element: any) => {
      if (element.color) colorsRange.push(element.color);
      // else {
      //   colorsRange.push(defaultColors[index]);
      //   index++;
      // }
    });
    this.colors = d3
      .scaleOrdinal()
      .domain(data.map((d: any) => d.value.toString()))
      .range(colorsRange);
    // Compute the position of each group on the pie:
    var pie = d3
      .pie()
      .startAngle(-120 * (Math.PI / 180))
      .endAngle(120 * (Math.PI / 180))

      .sort(null) // Do not sort group by size
      .value((d: any) => {
        return d.value;
      });
    var data_ready = pie(this.data);

    // The arc generator
    var arc = d3
      .arc()
      .cornerRadius(10)
      .innerRadius(this.radius * 0.6) // This is the size of the donut hole
      .outerRadius(this.radius * 0.7);


    //outer arc
    var outerArc = d3
      .arc()
      .innerRadius(this.radius * 0.9)
      .outerRadius(this.radius * 0.9);

    // Build the  chart
    this.svg
      .selectAll('allSlices')
      .data(data_ready)
      .enter()
      .append('path')
      .attr('d', arc)
      .attr('fill', (d: any) => this.colors(d.data.value))
      .attr('stroke', 'white')
      .style('stroke-width', '2px')
      .style('opacity', 0.7);

    this.svg
      .append('text')
      .text(this.commonService.liveData['ProcessedPalletQty'])
      .attr('x', '1')
      .attr('y', '0')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('font-weight', 'bold')
      .attr('font-size', '20px')
    // .attr('fill', '#0076ce');
    this.svg
      .append('text')
      .text('received')
      .attr('x', '1')
      .attr('y', '15')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('fill', 'green')
      .attr('font-size', '8px')
    this.svg
      .append('text')
      .text('quantity')
      .attr('x', '1')
      .attr('y', '25')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('fill', 'green')
      .attr('font-size', '8px')
    this.svg
      .append('text')
      .text(this.commonService.liveData['TotalPalletQty'])
      .attr('x', '-95')
      .attr('y', '0')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('font-weight', 'bold')
      .attr('font-size', '20px')
    // .attr('fill', '#0076ce');
    this.svg
      .append('text')
      .text('confirmed ')
      .attr('x', '-95')
      .attr('y', '15')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('fill', 'black')
      .attr('font-size', '8px')
    this.svg
      .append('text')
      .text('Pallet')
      .attr('x', '-95')
      .attr('y', '25')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('fill', 'black')
      .attr('font-size', '8px')
    this.svg
      .append('text')
      .text('quantity')
      .attr('x', '-95')
      .attr('y', '35')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('fill', 'black')
      .attr('font-size', '8px')

    this.svg
      .append('text')
      .text(this.commonService.liveData['PendingPalletQty'])
      .attr('x', '95')
      .attr('y', '0')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('font-weight', 'bold')
      .attr('font-size', '20px')
    // .attr('fill', '#0076ce');
    this.svg
      .append('text')
      .text('pending')
      .attr('x', '95')
      .attr('y', '15')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('fill', 'gray')
      .attr('font-size', '8px');
    this.svg
      .append('text')
      .text('quantity')
      .attr('x', '95')
      .attr('y', '25')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('fill', 'gray')
      .attr('font-size', '8px')
  }

  complete() {
    this.router.navigate(['home/loading/palletdetails']);

  }
  gotoStatuspage(status: any) {

    let message;
    let type;
    if (this.commonService.packingTypeValue == 'Box Level') {
      type = 'Box'
    }
    else {
      type = 'Pallet'
    }
    if (status == 1 && this.commonService?.confirmData?.['PalletQty'] == this.commonService.liveData['ProcessedPalletQty'] && this.commonService.liveData['ProcessedPalletQty'] != 0) {
      message = 'Do you want to complete the' + ' ' + type + '?'
    }
    if (status == 2 && this.commonService?.confirmData?.['PalletQty'] > this.commonService.liveData['ProcessedPalletQty'] && this.commonService.liveData['ProcessedPalletQty'] != 0) {

      message = 'Do you want to Accept Partial Pallet?'
    }
    if (status == 3 && this.commonService?.confirmData?.['PalletQty'] >= this.commonService.liveData['ProcessedPalletQty']) {
      message = 'Do you want to Reject  ' + ' ' + type + '?'
    }
    // if ((status == 1 && this.commonService?.confirmData?.['PalletQty'] > this.commonService.liveData['ProcessedPalletQty']) || (status == 2 && this.commonService?.confirmData?.['PalletQty'] > this.commonService.liveData['ProcessedPalletQty']) || (status == 3 && this.commonService?.confirmData?.['PalletQty'] > this.commonService.liveData['ProcessedPalletQty'])) {
    //   message = 'Please Check and close the Pallet'
    // }

    // if (this.commonService.liveData['ProcessedPalletQty'] == 0 || (status == 1 && this.commonService?.confirmData?.['PalletQty'] > this.commonService.liveData['ProcessedPalletQty'])) {
    //   message = 'Please Check and close the Pallet'
    // }
    let dialogRef = this.dialog.open(ConfirmationdialogComponent, {
      data: message
    })
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.statuscode = status;
        let body: any = {};
        body.PalletId = this.commonService.confirmData['PalletId'];
        body.PalletStatusId = this.statuscode;
        this.httpService.post('UpdatePalletStatus', body).subscribe((res: any) => {
          if (res) {
            this.ws.onclose = () => this.ws.send("connection Disconnected");
            this.commonService.palletstatus = true;
            this.commonService.isdisabled = false;

            this.router.navigate(['home/loading/skuscan'])
          }
        }, err => {
          this.dialog.open(ConfirmationdialogComponent, {
            data: 'something'
          });

        })
      }
      else {
        dialogRef.close();
      }

    })
  }
}


