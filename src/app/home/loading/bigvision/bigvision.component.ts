import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as d3 from 'd3';
import { CommonService } from 'src/app/services/common/common.service';
@Component({
  selector: 'app-bigvision',
  templateUrl: './bigvision.component.html',
  styleUrls: ['./bigvision.component.scss']
})
export class BigvisionComponent implements OnInit {
  // statusdata = [
  //   { status: 'Accepted' }
  // ]
  ispalletcompleted = false;
  // data: any = [
  //   {

  //     value: '2',
  //     color: '#1ABB1D',
  //   },
  //   {

  //     value: '0',
  //     color: '#DD1208',
  //   },
  //   {
  //     name: 'c',
  //     value: '6',
  //     color: '#CBC2C2',

  //   },
  // ];
  data: any = [];
  // startAngle: -Math.PI/4, endAngle: },
  private margin = { top: 10, right: 30, bottom: 30, left: 40 };
  private width = 250;
  private height = 250;
  private svg: any;
  private colors: any;
  private radius = Math.min(this.width, this.height) / 2 - this.margin.left;
  ws!: WebSocket;
  actual: any;
  target: any;
  palletstatus: any = 'null';
  isShow = true;
  constructor(private router: Router, public commonService: CommonService) {

    this.ws = new WebSocket('ws://win-he1pd6l1529:8080/');
    this.ws.onopen = () => this.ws.send("connected to Server");
    // this.isShow = false;

    this.ws.onmessage = async (event) => {

      if (event.data) {
        this.isShow = false;
        this.commonService.liveData = JSON.parse(event.data);
        if (this.commonService.liveData.PalletStatus !== null) {
          this.isShow = true;
          this.commonService.palletstatus = false;
        }
        else if (this.commonService.liveData.PalletStatus == null) {
          this.isShow = false;
          this.commonService.palletstatus = true;
        }

        this.palletstatus = this.commonService.liveData.PalletStatus
        let obj = { value: this.commonService.liveData['ProcessedPalletQty'], color: '#1ABB1D' };
        let obj1 = { value: this.commonService.liveData['PendingPalletQty'], color: '#CBC2C2' };
        let arr = [obj, obj1];
        this.actual = Math.round(commonService.liveData['ProcessedPalletQty'] / commonService.liveData['TotalPalletQty'] * 100)
        this.target = Math.round(commonService.liveData['PendingPalletQty'] / commonService.liveData['TotalPalletQty'] * 100)
        this.data = arr;
        d3.selectAll('svg').remove();
        await this.drawChart1(this.data);
      }


    }

    // // this.ws = new WebSocket('ws://172.16.1.234:8080/');
    // // this.ws.onopen = () => this.ws.send("connected to Server");

    // // this.ws.onmessage = async (event) => {
    // //   if (event.data) {
    // //     this.commonService.liveData = JSON.parse(event.data);
    // //     let obj = { value: this.commonService.liveData['ProcessedPalletQty'], color: '#1ABB1D' };
    // //     let obj1 = { value: this.commonService.liveData['PendingPalletQty'], color: '#CBC2C2' };
    // //     let arr = [obj, obj1];
    // //     this.actual = Math.round(commonService.liveData['ProcessedPalletQty'] / commonService.liveData['TotalPalletQty'] * 100)
    // //     this.target = Math.round(commonService.liveData['PendingPalletQty'] / commonService.liveData['TotalPalletQty'] * 100)
    // //     this.data = arr;
    // //     d3.select('svg').remove();

    // //     await this.drawChart1(this.data);
    // //     // this.createSvg();
    // //     // this.createColors(this.data);
    // //     // this.drawChart();
    // //   }

    // }
  }
  ngOnInit(): void {
    // this.isShow = false;
    this.actual = Math.round(this.commonService.liveData['ProcessedPalletQty'] / this.commonService.liveData['TotalPalletQty'] * 100)
    this.target = Math.round(this.commonService.liveData['PendingPalletQty'] / this.commonService.liveData['TotalPalletQty'] * 100)
    let obj = { value: this.commonService.liveData['ProcessedPalletQty'], color: '#1ABB1D' };
    let obj1 = { value: this.commonService.liveData['PendingPalletQty'], color: '#CBC2C2' };
    let arr = [obj, obj1];
    this.data = arr;
    this.palletstatus = 'null';

    // d3.select('svg').remove();
    // d3.selectAll('svg').remove();
    // this.drawChart1(this.data);


  }
  private drawChart1(data: any) {

    this.svg = d3
      .selectAll('#donutchart1')
      .append('svg')
      .attr('viewBox', `0 0 ${this.width} ${this.height}`)
      .append('g')
      .attr(
        'transform',
        'translate(' + this.width / 2 + ',' + this.height / 2 + ')'
      );
    const colorsRange: any = [];
    this.data.forEach((element: any) => {
      if (element.color) colorsRange.push(element.color);
      // else {
      //   colorsRange.push(defaultColors[index]);
      //   index++;
      // }
    });

    this.colors = d3

      .scaleOrdinal()
      .domain(data.map((d: any) => d.value.toString()))
      .range(colorsRange);
    // Compute the position of each group on the pie:
    var pie = d3
      .pie()
      .startAngle(-120 * (Math.PI / 180))
      .endAngle(120 * (Math.PI / 180))

      .sort(null) // Do not sort group by size
      .value((d: any) => {
        return d.value;
      });
    var data_ready = pie(this.data);

    // The arc generator
    var arc = d3
      .arc()
      .cornerRadius(10)
      .innerRadius(this.radius * 0.6) // This is the size of the donut hole
      .outerRadius(this.radius * 0.7);


    // Another arc that won't be drawn. Just for labels positioning
    var outerArc = d3
      .arc()
      .innerRadius(this.radius * 0.9)
      .outerRadius(this.radius * 0.9);

    // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
    this.svg
      .selectAll('allSlices')
      .data(data_ready)
      .enter()
      .append('path')
      .attr('d', arc)
      .attr('fill', (d: any) => this.colors(d.data.value))
      .attr('stroke', 'white')
      .style('stroke-width', '2px')
      .style('opacity', 0.7);

    this.svg
      .append('text')
      .text(this.commonService.liveData['ProcessedPalletQty'])
      .attr('x', '1')
      .attr('y', '0')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('font-weight', 'bold')
      .attr('font-size', '20px')
    // .attr('fill', '#0076ce');
    this.svg
      .append('text')
      .text('received')
      .attr('x', '1')
      .attr('y', '15')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('fill', 'green')
      .attr('font-size', '8px')
    this.svg
      .append('text')
      .text('quantity')
      .attr('x', '1')
      .attr('y', '25')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('fill', 'green')
      .attr('font-size', '8px')
    this.svg
      .append('text')
      .text(this.commonService.liveData['TotalPalletQty'])
      .attr('x', '-95')
      .attr('y', '0')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('font-weight', 'bold')
      .attr('font-size', '20px')
    // .attr('fill', '#0076ce');
    this.svg
      .append('text')
      .text('confirmed ')
      .attr('x', '-95')
      .attr('y', '15')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('fill', 'black')
      .attr('font-size', '8px')
    this.svg
      .append('text')
      .text('Pallet')
      .attr('x', '-95')
      .attr('y', '25')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('fill', 'black')
      .attr('font-size', '8px')
    this.svg
      .append('text')
      .text('quantity')
      .attr('x', '-95')
      .attr('y', '35')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('fill', 'black')
      .attr('font-size', '8px')

    this.svg
      .append('text')
      .text(this.commonService.liveData['PendingPalletQty'])
      .attr('x', '95')
      .attr('y', '0')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('font-weight', 'bold')
      .attr('font-size', '20px')
    // .attr('fill', '#0076ce');
    this.svg
      .append('text')
      .text('pending')
      .attr('x', '95')
      .attr('y', '15')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('fill', 'gray')
      .attr('font-size', '8px');
    this.svg
      .append('text')
      .text('quantity')
      .attr('x', '95')
      .attr('y', '25')
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'central')
      .attr('fill', 'gray')
      .attr('font-size', '8px')
  }



  // private createSvg(): void {
  //   // d3.select('svg').remove();
  //   this.svg = d3
  //     .select('#donutchart')
  //     .append('svg')
  //     .attr('viewBox', `0 0 ${this.width} ${this.height}`)
  //     .append('g')
  //     .attr(
  //       'transform',
  //       'translate(' + this.width / 2 + ',' + this.height / 2 + ')'
  //     );
  // }

  // private createColors(data: any): void {
  //   let index = 0;
  //   // const defaultColors = [
  //   //   '#1ABB1D ',
  //   //   '#DD1208',
  //   //   // '#6162b5',
  //   //   // '#6586f6',
  //   //   // '#8b6ced',
  //   //   // '#1b1b1b',
  //   //   // '#212121',
  //   // ];
  //   const colorsRange: any = [];
  //   this.data.forEach((element: any) => {
  //     if (element.color) colorsRange.push(element.color);
  //     // else {
  //     //   colorsRange.push(defaultColors[index]);
  //     //   index++;
  //     // }
  //   });
  //   this.colors = d3
  //     .scaleOrdinal()
  //     .domain(data.map((d: any) => d.value.toString()))
  //     .range(colorsRange);
  // }

  // private drawChart(): void {
  //   // Compute the position of each group on the pie:
  //   var pie = d3
  //     .pie()
  //     .startAngle(-120 * (Math.PI / 180))
  //     .endAngle(120 * (Math.PI / 180))

  //     .sort(null) // Do not sort group by size
  //     .value((d: any) => {
  //       return d.value;
  //     });
  //   var data_ready = pie(this.data);

  //   // The arc generator
  //   var arc = d3
  //     .arc()
  //     .cornerRadius(10)
  //     .innerRadius(this.radius * 0.6) // This is the size of the donut hole
  //     .outerRadius(this.radius * 0.7);


  //   // Another arc that won't be drawn. Just for labels positioning
  //   var outerArc = d3
  //     .arc()
  //     .innerRadius(this.radius * 0.9)
  //     .outerRadius(this.radius * 0.9);

  //   // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
  //   this.svg
  //     .selectAll('allSlices')
  //     .data(data_ready)
  //     .enter()
  //     .append('path')
  //     .attr('d', arc)
  //     .attr('fill', (d: any) => this.colors(d.data.value))
  //     .attr('stroke', 'white')
  //     .style('stroke-width', '2px')
  //     .style('opacity', 0.7);

  //   this.svg
  //     .append('text')
  //     .text(this.commonService.liveData['ProcessedPalletQty'])
  //     .attr('x', '1')
  //     .attr('y', '0')
  //     .style('text-anchor', 'middle')
  //     .style('dominant-baseline', 'central')
  //     .attr('font-weight', 'bold')
  //     .attr('font-size', '20px')
  //   // .attr('fill', '#0076ce');
  //   this.svg
  //     .append('text')
  //     .text('received')
  //     .attr('x', '1')
  //     .attr('y', '15')
  //     .style('text-anchor', 'middle')
  //     .style('dominant-baseline', 'central')
  //     .attr('fill', 'green')
  //     .attr('font-size', '8px')
  //   this.svg
  //     .append('text')
  //     .text('quantity')
  //     .attr('x', '1')
  //     .attr('y', '25')
  //     .style('text-anchor', 'middle')
  //     .style('dominant-baseline', 'central')
  //     .attr('fill', 'green')
  //     .attr('font-size', '8px')
  //   this.svg
  //     .append('text')
  //     .text(this.commonService.liveData['TotalPalletQty'])
  //     .attr('x', '-95')
  //     .attr('y', '0')
  //     .style('text-anchor', 'middle')
  //     .style('dominant-baseline', 'central')
  //     .attr('font-weight', 'bold')
  //     .attr('font-size', '20px')
  //   // .attr('fill', '#0076ce');
  //   this.svg
  //     .append('text')
  //     .text('confirmed ')
  //     .attr('x', '-95')
  //     .attr('y', '15')
  //     .style('text-anchor', 'middle')
  //     .style('dominant-baseline', 'central')
  //     .attr('fill', 'black')
  //     .attr('font-size', '8px')
  //   this.svg
  //     .append('text')
  //     .text('Pallet')
  //     .attr('x', '-95')
  //     .attr('y', '25')
  //     .style('text-anchor', 'middle')
  //     .style('dominant-baseline', 'central')
  //     .attr('fill', 'black')
  //     .attr('font-size', '8px')
  //   this.svg
  //     .append('text')
  //     .text('quantity')
  //     .attr('x', '-95')
  //     .attr('y', '35')
  //     .style('text-anchor', 'middle')
  //     .style('dominant-baseline', 'central')
  //     .attr('fill', 'black')
  //     .attr('font-size', '8px')

  //   this.svg
  //     .append('text')
  //     .text(this.commonService.liveData['PendingPalletQty'])
  //     .attr('x', '95')
  //     .attr('y', '0')
  //     .style('text-anchor', 'middle')
  //     .style('dominant-baseline', 'central')
  //     .attr('font-weight', 'bold')
  //     .attr('font-size', '20px')
  //   // .attr('fill', '#0076ce');
  //   this.svg
  //     .append('text')
  //     .text('pending')
  //     .attr('x', '95')
  //     .attr('y', '15')
  //     .style('text-anchor', 'middle')
  //     .style('dominant-baseline', 'central')
  //     .attr('fill', 'gray')
  //     .attr('font-size', '8px');
  //   this.svg
  //     .append('text')
  //     .text('quantity')
  //     .attr('x', '95')
  //     .attr('y', '25')
  //     .style('text-anchor', 'middle')
  //     .style('dominant-baseline', 'central')
  //     .attr('fill', 'gray')
  //     .attr('font-size', '8px')

  // }

  // }
  // constructor() { }

  // ngOnInit(): void {
  // }

}
