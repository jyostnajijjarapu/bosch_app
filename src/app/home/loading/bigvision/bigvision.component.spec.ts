import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BigvisionComponent } from './bigvision.component';

describe('BigvisionComponent', () => {
  let component: BigvisionComponent;
  let fixture: ComponentFixture<BigvisionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BigvisionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BigvisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
