import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  tableData = [
    {
      date: '1st Dec 2022', time: '10.03 AM', shift: 'Shift A', partNo: '983938A', palletId: '101101', partQtyPlan: '140', partQtyActual: '135', status: 'Reject'
    },
    {
      date: '1st Dec 2022', time: '10.03 AM', shift: 'Shift A', partNo: '983938A', palletId: '101105', partQtyPlan: '140', partQtyActual: '140', status: 'Completed'
    },
    {
      date: '1st Dec 2022', time: '10.03 AM', shift: 'Shift A', partNo: '983938A', palletId: '101101', partQtyPlan: '130', partQtyActual: '120', status: 'PartialClosed'
    },
    {
      date: '1st Dec 2022', time: '10.03 AM', shift: 'Shift A', partNo: '983938A', palletId: '101102', partQtyPlan: '140', partQtyActual: '100', status: 'NotCompleted'
    },
    {
      date: '1st Dec 2022', time: '10.03 AM', shift: 'Shift A', partNo: '983938A', palletId: '101103', partQtyPlan: '140', partQtyActual: '120', status: 'NotCompleted'
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
