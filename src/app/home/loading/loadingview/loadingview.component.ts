import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common/common.service';
import { ConfirmLoadDataComponent } from 'src/app/confirm-load-data/confirm-load-data.component';
import { ConfirmationdialogComponent } from 'src/app/confirmationdialog/confirmationdialog.component';
import { HttpService } from 'src/app/services/http/http.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-loadingview',
  templateUrl: './loadingview.component.html',
  styleUrls: ['./loadingview.component.scss']
})
export class LoadingviewComponent implements OnInit {
  @ViewChild('tnt') tnt: any;
  // hide = true;
  // in = false;
  // store: any;
  isEdit = false;
  // isEdit1 = false;
  totalBoxes: any;
  boxesQty: any;
  isStart = true;
  // boxcount = 7
  // boxqty = 20
  // matrixData = [
  //   { boxQty: 20, palletPartQty: 140, pallet_index: 103, part_number: '10456969A' }

  // ]
  total_boxes: any;
  isPalletLevel = false;
  isBoxLevel = false;
  isProdLine = false;
  isunfilled = false;
  barcodeData: any = [];
  fgPartNumber: any;
  outboundfeed: any = '';
  packingType: any = '';
  modalData: any;
  isLoading = false;
  isValues = false;
  ws!: WebSocket;
  fgpartList: any = [];
  filteredpartNumbers: any = [];
  constructor(private router: Router, private elementRef: ElementRef, private httpService: HttpService, public dialog: MatDialog, private commonService: CommonService) {
    document.addEventListener('keydown', function (event: any) {
      if (event.keyCode == 13 || event.keyCode == 17 || event.keyCode == 74)
        event.preventDefault();
    });

  }
  @ViewChild("myinput") myInputField!: ElementRef;
  ngAfterViewInit() {
    this.myInputField.nativeElement.focus();
  }
  ngOnInit(): void {
    this.httpService.get('GetFGPartNumberOriginal').subscribe((res: any) => {
      if (res) {

        res.forEach((element: any) => {
          console.log(element, "ele");
          this.fgpartList.push(element['FGPartNumberOriginal']);
          this.filteredpartNumbers = this.fgpartList;
        });
      }
    }, err => {
      this.dialog.open(ConfirmationdialogComponent, {
        data: 'something'
      });
      // this.isLoading = false;
    })
  }
  onKey(value: any) {
    this.filteredpartNumbers = [];
    this.search(value);
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.fgpartList.filter = filterValue.trim().toLowerCase();
  }
  search(value: any) {
    let filter = value.toLowerCase();
    for (let i = 0; i < this.fgpartList.length; i++) {
      let option = this.fgpartList[i];
      if (option.toLowerCase().indexOf(filter) >= 0) {
        this.filteredpartNumbers.push(option);
      }
    }
    // return this.fgpartList.filter((option: any) => option.toLowerCase().startsWith(filter));
  }
  edit() {
    this.isEdit = true;
  }
  // edit1() {
  //   this.isEdit1 = true;
  // }
  changepart(event: any) {
    this.fgPartNumber = event;

    if (this.fgPartNumber !== undefined) {
      this.getbarcodeData();
    }
    else {
      console.log("elseblock")
    }

  }
  getbarcodeData() {
    this.isLoading = true;
    this.httpService.get('GetPalletMatrix?Id=' + this.fgPartNumber).subscribe((res: any) => {
      if (res) {
        this.commonService.isdisabled = true;

        this.barcodeData = res;
        this.isStart = false;
        this.isLoading = false;
      }
      else {
        this.isLoading = false;
        const dialogRef = this.dialog.open(ConfirmationdialogComponent, {
          data: "Data not found.Please scan again..."
        });
        dialogRef.afterClosed().subscribe((confirmed: boolean) => {
          if (confirmed) {

            this.router.navigate(['home/loading/skuscan'])
          }
        })
        this.isStart = true;

      }

      // this.pallet_qty = res.pallet_qty;
    }, err => {
      this.dialog.open(ConfirmationdialogComponent, {
        data: 'something'
      });
      this.isLoading = false;
    })
  }
  save() {
    this.isEdit = false;
    this.barcodeData['TotalBoxes'] = this.totalBoxes;
  }
  // save1() {
  //   this.isEdit1 = false;
  //   this.barcodeData['BoxQty'] = this.boxesQty;
  // }
  palletlevel() {
    this.isPalletLevel = !this.isPalletLevel;
    this.isBoxLevel = false;
    this.packingType = 'Pallet Level'
    this.isValues = false;
  }
  boxlevel() {
    this.isBoxLevel = !this.isBoxLevel;
    this.isPalletLevel = false;
    this.packingType = 'Box Level';
    this.isValues = false;

  }
  prodline() {
    this.isProdLine = !this.isProdLine;
    this.isunfilled = false;
    this.outboundfeed = 'Production Line';
    this.isValues = false;

  }
  unfilledArea() {
    this.isunfilled = !this.isunfilled;
    this.isProdLine = false;
    this.outboundfeed = 'Unfilled Area';
    this.isValues = false;

  }
  scanAgain() {

    const dialogRef = this.dialog.open(ConfirmationdialogComponent, {
      data: "Are you sure want to scan again?"
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.commonService.isdisabled = false;
        this.isProdLine = false;
        this.isunfilled = false;
        this.isBoxLevel = false;
        this.isPalletLevel = false;
        this.router.navigate(['home/loading/skuscan'])
      }
    })

  }
  loadstatus() {
    if (this.outboundfeed != '' && this.packingType != '') {

      this.isLoading = true;
      this.modalData = this.barcodeData;
      this.modalData.outboundfeed = this.outboundfeed;
      this.modalData.pakingType = this.packingType;
      const dialogRef = this.dialog.open(ConfirmLoadDataComponent, {
        data: this.modalData
      });
      dialogRef.afterClosed().subscribe((confirmed: boolean) => {
        if (confirmed) {
          this.commonService.packingTypeValue = this.modalData.pakingType;
          let body: any = {};
          body['FGPartNumber'] = this.modalData.FGPartNumber;
          body['PalletQty'] = this.modalData.PalletQty;
          body['TotalBoxes'] = this.modalData.TotalBoxes;
          body['BoxQty'] = this.modalData.BoxQty;
          body['OutboundFeed'] = this.modalData.outboundfeed;
          body['PackingType'] = this.modalData.pakingType;
          body['ShiftTypeId'] = localStorage.getItem('CurrentShift');
          body['OperatorId'] = localStorage.getItem('user');
          this.httpService.post('InsertPalletData', body).subscribe((res: any) => {

            if (res) {
              this.isLoading = true;
              this.commonService.confirmData = res;
              // this.ws = new WebSocket('ws://172.16.1.234:8080/');
              this.router.navigate(['home/loading/palletloadstatus'])
            }
            else {
              this.isLoading = false;
            }
          }, err => {
            this.dialog.open(ConfirmationdialogComponent, {
              data: 'something'
            });
            this.isLoading = false;
          })


        }
        else {
          this.isLoading = false;
        }
        // else {

        //   const dialogRef = this.dialog.open(ConfirmationdialogComponent, {
        //     data: "Are you sure want to scan again?"
        //   });
        //   dialogRef.afterClosed().subscribe((confirmed: boolean) => {
        //     if (confirmed) {
        //       this.isProdLine = false;
        //       this.isunfilled = false;
        //       this.isBoxLevel = false;
        //       this.isPalletLevel = false;
        //       this.router.navigate(['home/loading/skuscan'])
        //     }
        //   })
        // }
      });

    }
    else {
      this.isValues = true;
    }



  }
}
