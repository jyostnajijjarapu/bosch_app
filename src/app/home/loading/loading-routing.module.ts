import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BigvisionComponent } from './bigvision/bigvision.component';
import { LoadingviewComponent } from './loadingview/loadingview.component';
import { PalletMatrixComponent } from './pallet-matrix/pallet-matrix.component';
import { PalletStatusComponent } from './pallet-status/pallet-status.component';
import { PalletdetailsComponent } from './palletdetails/palletdetails.component';
import { PalletloadstatusComponent } from './palletloadstatus/palletloadstatus.component';
import { ReportsComponent } from './reports/reports.component';
import { SkuscanComponent } from './skuscan/skuscan.component';

const routes: Routes = [
  { path: 'skuscan', component: SkuscanComponent },
  { path: 'loadingview', component: LoadingviewComponent },
  { path: 'palletloadstatus', component: PalletloadstatusComponent },
  { path: 'palletdetails', component: PalletdetailsComponent },
  { path: 'palletmatrix', component: PalletMatrixComponent },
  { path: 'palletstatus', component: PalletStatusComponent },
  { path: 'reports', component: ReportsComponent },
  { path: 'bigvision', component: BigvisionComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoadingRoutingModule { }
