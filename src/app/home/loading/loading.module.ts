import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormGroupDirective, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoadingRoutingModule } from './loading-routing.module';
import { SkuscanComponent } from './skuscan/skuscan.component';
import { LoadingviewComponent } from './loadingview/loadingview.component';
import { PalletloadstatusComponent } from './palletloadstatus/palletloadstatus.component';
import { PalletdetailsComponent } from './palletdetails/palletdetails.component';
import { MaterialModule } from 'src/app/material.module';
import { PalletStatusComponent } from './pallet-status/pallet-status.component';
import { ConfirmLoadDataComponent } from '../../confirm-load-data/confirm-load-data.component';
import { PalletMatrixComponent } from './pallet-matrix/pallet-matrix.component';
import { BigvisionComponent } from '../loading/bigvision/bigvision.component';
import { PalletMatrixModalComponent } from './pallet-matrix/pallet-matrix-modal/pallet-matrix-modal.component';
import { MatSelectFilterModule } from 'mat-select-filter';

@NgModule({
  declarations: [
    SkuscanComponent, LoadingviewComponent, PalletloadstatusComponent, PalletdetailsComponent, PalletStatusComponent, ConfirmLoadDataComponent, PalletMatrixComponent, BigvisionComponent, PalletMatrixModalComponent
  ],
  imports: [
    CommonModule,
    LoadingRoutingModule, MaterialModule, FormsModule, ReactiveFormsModule, MatSelectFilterModule
  ]
})
export class LoadingModule { }
