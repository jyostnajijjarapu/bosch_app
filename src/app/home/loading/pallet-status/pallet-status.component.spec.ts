import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PalletStatusComponent } from './pallet-status.component';

describe('PalletStatusComponent', () => {
  let component: PalletStatusComponent;
  let fixture: ComponentFixture<PalletStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PalletStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PalletStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
