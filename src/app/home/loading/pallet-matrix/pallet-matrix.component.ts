import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { HttpService } from 'src/app/services/http/http.service';
import { PalletMatrixModalComponent } from './pallet-matrix-modal/pallet-matrix-modal.component';
import { ConfirmationdialogComponent } from 'src/app/confirmationdialog/confirmationdialog.component';
import { CommonService } from 'src/app/services/common/common.service';

export interface skuProdRunData {
  PalletMatrixId: number;
  FGPartNumberOriginal: string;
  FGPartNumber: string;
  BoxQty: number;
  PalletQty: number;
  PackingIndex: string;
}


@Component({
  selector: 'app-pallet-matrix',
  templateUrl: './pallet-matrix.component.html',
  styleUrls: ['./pallet-matrix.component.scss']
})
export class PalletMatrixComponent implements OnInit {
  displayedColumns: string[] = ['PalletMatrixId', 'FGPartNumberOriginal', 'FGPartNumber', 'BoxQty', 'PalletQty', 'PackingIndex', 'action'];
  dataSource = new MatTableDataSource();
  pagesizeOptions: number[] = [10, 20, 50];
  @ViewChild(MatTable, { static: true }) table!: MatTable<any>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  isLoading = false;
  constructor(public dialog: MatDialog, private httpService: HttpService, private commonService: CommonService) {

  }


  ngOnInit(): void {
    this.getAllRecords();
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  //open Modal
  openModal(event: any, row: any) {
    console.log(row, event, "openmodal")
    this.commonService.changeMessage(event);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "20%";
    dialogConfig.maxHeight = "600px";
    dialogConfig.data = row;
    const dialogRef = this.dialog.open(PalletMatrixModalComponent, dialogConfig)

    dialogRef.afterClosed().subscribe(result => {
      console.log("result", result);

      if (result.event == 'Add') {
        this.addRowData(result.data);
      }
      else if (result.event == 'Update') {
        this.updateRowData(result.data);
      }
    });


  }
  // get palletmatrix
  getAllRecords() {
    this.isLoading = true;

    this.httpService.get('GetPalletMatrixDetails').subscribe((response: any) => {
      this.isLoading = false;
      this.dataSource.paginator = this.paginator;
      let res: any = response;
      if (res) {
        console.log(res[0]['PalletQty'], "palletres")
        this.dataSource.data = res;

      } else {
        this.dialog.open(ConfirmationdialogComponent, {
          data: 'norecords'
        });
      }

    }), (err: any) => {
      this.dialog.open(ConfirmationdialogComponent, {
        data: 'something'
      });
      this.isLoading = false;
    };

  };


  //Add palletmatrix record
  addRowData(row_obj: any) {

    // let insertData = {
    //   "FGPartNumberOriginal": row_obj.fgPartNumber,
    //   "FGPartNumber": row_obj.partNumber,
    //   "BoxQty": row_obj.boxQty,
    //   "PalletQty": row_obj.palletQty,
    //   "PackingIndex": row_obj.packingIndex
    // }
    this.isLoading = true;
    this.httpService.post('InsertPalletMatrix', row_obj).subscribe((response: any) => {

      if (response) {
        this.dialog.open(ConfirmationdialogComponent, {
          data: 'Record Added Successfully'
        });
        this.getAllRecords();
      } else {
        this.dialog.open(ConfirmationdialogComponent, {
          data: 'something'
        });
        this.isLoading = false;
      }
      this.isLoading = false;
    }), (err: any) => {
      this.dialog.open(ConfirmationdialogComponent, {
        data: 'something'
      });
      this.isLoading = false;
    };



  }

  //Edit the palletmatrix record

  updateRowData(row_obj: any) {
    console.log(row_obj, "updaterow")
    this.httpService.post('UpdatePalletMatrix', row_obj).subscribe((response: any) => {

      if (response) {
        this.dialog.open(ConfirmationdialogComponent, {
          data: 'Record Updated Successfully'
        });
        this.getAllRecords();
      } else {
        this.dialog.open(ConfirmationdialogComponent, {
          data: 'something'
        });
        this.isLoading = false;
      }
      this.isLoading = false;
    }), (err: any) => {
      this.dialog.open(ConfirmationdialogComponent, {
        data: 'something'
      });
      this.isLoading = false;
    };
  }

  //Delete row
  deleteRowData(row_obj: any) {


    console.log(row_obj, "deleterow")
    let deleteData = {
      "PalletMatrixId": row_obj.PalletMatrixId
    }
    const dialogRef = this.dialog.open(ConfirmationdialogComponent, {
      data: 'Are you sure want to delete?'
    });
    dialogRef.afterClosed().subscribe((closeType: any) => {
      if (closeType) {

        this.httpService.post('DeletePalletMatrix', deleteData).subscribe((response) => {
          if (response) {
            console.log("deleted record")
            this.dialog.open(ConfirmationdialogComponent, {
              data: 'Record Deleted Successfully'
            });
            this.getAllRecords();
          } else {
            this.dialog.open(ConfirmationdialogComponent, {
              data: 'something'
            });
            this.isLoading = false;
          }
          this.isLoading = false;
        }), (err: any) => {
          this.dialog.open(ConfirmationdialogComponent, {
            data: 'something'
          });
          this.isLoading = false;
        };
      }

    });
  }

}
