import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { BehaviorSubject } from 'rxjs';
import { CommonService } from 'src/app/services/common/common.service';

export class UsersData {
    PalletMatrixId!: number;
    FGPartNumberOriginal!: string;
    FGPartNumber!: string;
    BoxQty!: number;
    PalletQty!: number;
    PackingIndex!: string;

}

@Component({
    selector: 'app-pallet-matrix-modal',
    templateUrl: './pallet-matrix-modal.component.html',
    styleUrls: ['./pallet-matrix-modal.component.scss']
})
export class PalletMatrixModalComponent implements OnInit {
    palletForm: FormGroup;
    message: string = '';
    required = 'This field is required';
    isHidden: boolean = false;

    constructor(
        private formBuilder: FormBuilder, public dialogRef: MatDialogRef<PalletMatrixModalComponent>, private commonService: CommonService,
        @Optional() @Inject(MAT_DIALOG_DATA) public data: UsersData
    ) {
        this.palletForm = this.formBuilder.group({
            PalletMatrixId: [''],
            FGPartNumberOriginal: ['', Validators.required],
            FGPartNumber: ['', Validators.required],
            BoxQty: ['', Validators.required],
            PalletQty: ['', Validators.required],
            PackingIndex: ['', Validators.required]

        });
        if (Object.keys(data).length === 0 && data.constructor === Object) {
            this.palletForm.reset();
            this.setToDefault();
        } else {
            this.palletForm.patchValue(data)
            this.palletForm.value;
        }


    }

    ngOnInit() {

        this.commonService.currentMessage.subscribe((message: any) => {
            this.message = message
            this.isHidden = true;
        })
    }

    submit() {

        if (!this.palletForm.valid) {
            return;
        }
        this.dialogRef.close({ event: this.message, data: this.palletForm.value });
    }


    setToDefault() {
        this.palletForm = this.formBuilder.group({
            PalletMatrixId: [''],
            FGPartNumberOriginal: ['', Validators.required],
            FGPartNumber: ['', Validators.required],
            BoxQty: ['', Validators.required],
            PalletQty: ['', Validators.required],
            PackingIndex: ['', Validators.required]
        });
    }

    onClose() {
        this.palletForm.reset();
        this.setToDefault();
        this.dialogRef.close({ event: 'Cancel' });
    }

}
