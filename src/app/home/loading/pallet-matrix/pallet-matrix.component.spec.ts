import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PalletMatrixComponent } from './pallet-matrix.component';

describe('PalletMatrixComponent', () => {
  let component: PalletMatrixComponent;
  let fixture: ComponentFixture<PalletMatrixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PalletMatrixComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PalletMatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
