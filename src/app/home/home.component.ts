import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatAccordion } from '@angular/material/expansion';
import { ActivatedRoute, Router } from '@angular/router';
import { menuItems } from '../sidemenu';
import { ConfirmationdialogComponent } from '../confirmationdialog/confirmationdialog.component';
import { timer } from 'rxjs';
import { ChangepasswordComponent } from '../changepassword/changepassword.component';
import { HttpService } from '../services/http/http.service';
import { CommonService } from '../services/common/common.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  menuOptions: any = JSON.parse(JSON.stringify(menuItems));
  isMenuOpen: boolean = false;
  loggedInMailId: any;
  @ViewChild(MatAccordion)
  accordion: MatAccordion = new MatAccordion;
  username: any;
  profile: any;
  breadcrum: any;
  shift: any;
  date: any;
  isChangePwd = false;
  constructor(public router: Router, private activatedRoute: ActivatedRoute, public dialog: MatDialog, private httpService: HttpService, private common: CommonService) {



  }

  ngOnInit() {
    timer(0, 1000).subscribe(() => {
      this.date = new Date();
      // this.timestamp = Math.floor(Date.now() / 1000)

      const hours = this.date.getHours();
      if (hours >= 6 && hours < 15) {
        this.shift = 1
      }
      else if (hours >= 15 && hours < 24) {
        this.shift = 2;
      }
      else {
        this.shift = 3
      }
      localStorage.setItem('CurrentShift', this.shift);
    })
    let fname = localStorage.getItem('fname')
    let lname = localStorage.getItem('lname');
    this.username = fname + '' + lname;
  }

  // toggle menu handler
  toggleMenu(isOpened?: boolean) {
    if (isOpened) {
      this.isMenuOpen = true;
    } else {
      this.isMenuOpen = !this.isMenuOpen;

    }
  }

  // Make side menu item active when route changes
  applyActiveLinkBasedOnRoute(menuItem: any) {
    return this.router.url.includes(menuItem.routerLink);
  }
  //logout
  loggedout() {

    const dialogRef = this.dialog.open(ConfirmationdialogComponent, { data: "Are you sure want to logout?" });

    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.router.navigate(['/login'])
      }
      else {
        dialogRef.close();
      }
    });
  }
  profileopen() {
    this.isChangePwd = true;
  }
  changePassword() {
    const dialogRef = this.dialog.open(ChangepasswordComponent);
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        let body: any = {};
        body.UserId = this.common.userId;
        body.Password = confirmed;
        this.httpService.post('ChangePassword', body).subscribe((res) => {
          if (res) {
            this.router.navigate(['/login'])
          }
        })

      }
      else {
        dialogRef.close();
      }
    })
  }
}
