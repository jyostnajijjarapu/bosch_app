import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ConfirmationdialogComponent } from '../confirmationdialog/confirmationdialog.component';
import { HttpService } from '../services/http/http.service';
export interface DialogData {
  FGPartNumber: any;
  PalletQty: any;
  TotalBoxes: any;
  BoxQty: any;
  outboundfeed: any;
  pakingType: any;
  PackingIndex: any;
}
@Component({
  selector: 'app-confirm-load-data',
  templateUrl: './confirm-load-data.component.html',
  styleUrls: ['./confirm-load-data.component.scss']
})
export class ConfirmLoadDataComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData, private dialogRef: MatDialogRef<ConfirmLoadDataComponent>, private router: Router, public dialog: MatDialog) { }

  ngOnInit(): void {
  }
  onConfirmClick() {

    this.dialogRef.close(true);

  }
  scanAgain() {
    const dialogRef = this.dialog.open(ConfirmationdialogComponent, {
      data: "Are you sure want to scan again?"
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.dialogRef.close(true);
        this.router.navigate(['home/loading/skuscan'])
        this.dialogRef.close(false);
      }

    })

  }
  closeConfirm() {
    this.dialogRef.close();
  }
}
