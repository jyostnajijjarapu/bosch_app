import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmLoadDataComponent } from './confirm-load-data.component';

describe('ConfirmLoadDataComponent', () => {
  let component: ConfirmLoadDataComponent;
  let fixture: ComponentFixture<ConfirmLoadDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmLoadDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmLoadDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
