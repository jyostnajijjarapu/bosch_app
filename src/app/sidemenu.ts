
export const menuItems = [
    {
        title: 'Order Scan',
        routerLink: 'loading/skuscan',
        icon: 'center_focus_weak',
    },
    {
        title: 'Loading View',
        routerLink: 'loading/loadingview',
        icon: 'update',
    },

    // {
    //     title: 'Logs',
    //     routerLink: 'logs',
    //     icon: 'dashboard',
    //     subMenu: [
    //         {
    //             title: 'Pallet or Box Log',
    //             routerLink: 'loading/palletorboxlog',
    //             icon: 'dashboard',
    //         },
    //         {
    //             title: 'Vision Log',
    //             routerLink: 'visionlog',
    //             icon: 'dashboard',
    //         },
    //         {
    //             title: 'Machine Log',
    //             routerLink: 'machinelog',
    //             icon: 'dashboard',
    //         },
    //     ]
    // },
    {
        title: 'PalletStatus Screen',
        routerLink: 'loading/palletloadstatus',
        icon: 'inbox',
    },
    // {
    //     title: 'Screen',
    //     routerLink: 'vision/bigscreen',
    //     icon: 'history',
    // },
    {
        title: 'Screen',
        routerLink: 'loading/bigvision',
        icon: 'history',
    },
    {
        title: 'Vision Log',
        routerLink: 'visionlog',
        icon: 'center_focus_strong',
    },
    {
        title: 'Machine Log',
        routerLink: 'machinelog',
        icon: 'settings',
    },
    {
        title: 'Pallet or Box Log',
        routerLink: 'loading/palletorboxlog',
        icon: 'redeem',
    },

    {
        title: 'Pallet Matrix',
        routerLink: 'loading/palletmatrix',
        icon: 'dashboard',
    },

    {
        title: 'Master Data',
        routerLink: 'masterData',
        icon: 'show_chart',
    },
    {
        title: 'Reports',
        routerLink: 'loading/reports',
        icon: 'report',
    },
    {
        title: 'Help',
        routerLink: 'help',
        icon: 'help'
    }
]