import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangepasswordComponent implements OnInit {
  hide = true;
  hide1 = true;
  hide2 = true;
  pwdForm: FormGroup | any;
  constructor(public router: Router, private fb: FormBuilder, private dialogRef: MatDialogRef<ChangepasswordComponent>) {
    this.pwdForm = this.fb.group({
      pwd: ['', Validators.required],
      newpwd: ['', Validators.required],
      cnewpwd: ['', Validators.required]
    })
  }

  ngOnInit(): void {
  }
  onCancel() {
    this.dialogRef.close();
  }
  onSubmit() {

    this.dialogRef.close(this.pwdForm.get('newpwd').value);

  }
}
